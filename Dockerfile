FROM tensorflow/tensorflow:1.12.3-gpu-py3

WORKDIR /app

COPY . /app


RUN pip install --upgrade pip &&\
    pip install keras==2.2.4 &&\
    pip install numpy &&\
    pip install scipy &&\
    pip install matplotlib &&\ 
    pip install six &&\
    pip install tifffile>=2020.5.11 &&\
    pip install tqdm &&\
    pip install csbdeep==0.4.0 &&\
    pip install Pillow &&\
    pip install ruamel.yaml>=0.16.10 &&\
    pip install --no-deps n2v

ENV PATH="/app:$PATH"

CMD ["bash"]
